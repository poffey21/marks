An easy place to find demonstrations that can be used to show off certain features of GitLab.

When a GitLab Feature relies on another tool, the other tool is called it after the forward-slash ` / `.

## GitLab Playgrounds

##### On SaaS

[GitLab Examples](https://gitlab.com/gitlab-examples) | [GitLab Free](https://gitlab.com/gitlab-free/) | [GitLab Bronze](https://gitlab.com/gitlab-bronze/) | [GitLab Silver](https://gitlab.com/gitlab-silver/) | [GitLab Gold](https://gitlab.com/gitlab-gold/) | [Guided Explorations](https://gitlab.com/guided-explorations/) | [FPotter](https://gitlab.com/fpotter/examples) | [GitLab CS Tools](https://gitlab.com/gitlab-cs-tools/) | [Reference Implementations](https://gitlab.com/reference-implementation) | [US PubSec](https://gitlab.com/us-public-sector/cs/demos/) | [Brownfield Dev](https://gitlab.com/brownfield-dev) | [Trustful Finance Demo](https://gitlab.com/trustful-finance-demo) | [The Atomic Lab](https://gitlab.com/the-atomic-lab) | [Tech Marketing](https://gitlab.com/tech-marketing) | [Golden Repos](https://gitlab.com/gitlab-com/customer-success/demos/golden-repos) | [Protect Demo](https://gitlab.com/gitlab-protect-demo)

##### On Self-Managed

[GitLab Demos](http://gitlabdemo.com/) | [GitLab Ninja](https://gitlab.gitlab.ninja/) | [GitLab Geo](https://geo.glcs.cloud/)

## GitLab UX

#### Manage

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/manage/)

* **Audit Events**: [Demo](#)
* **Subgroups**: [Demo](#) | [Trustful Finance Demo](https://www.youtube.com/watch?v=VR2r1TJCDew) and [Repo](https://gitlab.com/trustful-finance-demo) | [SAFe and Agile Planning Demo](https://www.youtube.com/watch?v=PmFFlTH2DQk) and [Repo](https://gitlab.com/healthcare-provider-inc)
* **Single Sign-On / SAML**: [Example](https://gitlab.com/groups/gitlab-silver/)
* **Code Analytics**: [Demo](#)
* **DevOps Score**: [Demo](#)
* **Value Stream Management**: [Demo](#)
* **Compliance Controls**: [Demo](#)

#### Plan

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/plan/)

* **Issue Tracking**: [Demo](#)
* **Kanban Boards**: [Demo](#) | [Trustful Finance Demo](https://gitlab.com/trustful-finance-demo)
* **Time Tracking**: [Demo](#)
* **Epics**: [Demo](#)
* **Roadmaps**: [Demo](#)
* **Requirements Management**: [Demo](#)
* **Quality Management**: [Demo](#)
* **Service Desk**: [Demo](#)
* **Jira Integration / Jira**: [Joel-4min](https://www.youtube.com/watch?v=Jn-_fyra7xQ) | [Adriano Fonseca](https://www.youtube.com/watch?v=p56zrZtrhQE) | [Adriano Fonseca JIRA Smart Commits](https://www.youtube.com/watch?v=y74brTo5EBA) | [GitLab Docs](https://docs.gitlab.com/ee/user/project/integrations/jira.html) | [Dev Panel](https://docs.gitlab.com/ee/integration/jira_development_panel.html) | [Blog Post](https://www.ilovedevops.com/post/2019-08-02-jira-two-way-integration/)

#### Create

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/create/)

* **Source Code Management**: [Demo](#)
* **Code Review**: [Demo](#)
* **Design Management**: [Demo](#)
* **Wiki**: [Demo](#)
* **Static Site Editor**: [Demo](#)
* **Web IDE**: [Demo](#)
* **Snippets**: [Demo](#)
* **Live Coding**: [Demo](#)

## CI/CD UX

#### Verify

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/verify/)

* **Continuous Integration**: [Demo](#)
* **Code Quality / Code Climate**: [Demo](#)   
* **Code Quality / Custom Reports**: [Docs](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html#implementing-a-custom-tool)
* **Unit Testing / JUnit Syntax**: [GitLab Ops](https://ops.gitlab.net/gitlab-org/quality/staging/pipelines/112686/test_report)
* **Integration Testing**: [Demo](#)
* **Load Testing / k6**: [Demo](#)
* **Web Performance / Sitespeed**: [Demo](#)
* **System Testing**: [Demo](#)
* **Usability Testing**: [Demo](#)
* **Accessibility Testing**: [Demo](#)
* **Metrics Reports / OpenMetrics**: [Demo](#)
* **Security Reports / Reports JSON**: [Docs](https://docs.gitlab.com/ee/user/application_security/container_scanning/#reports-json-format)

#### Package 

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/package/)

* **Package Registry**: [Demo](#)
* **Container Registry**: [Demo](#)
* **Helm Chart Registry**: [Demo](#)
* **Dependency Proxy**: [Demo](#)
* **Dependency Firewall**: [Demo](#)

#### Secure

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/secure/)

* **SAST / Language Specific**: [Demo](#)
* **DAST / OWASP ZAProxy**: [Demo](#)
* **Secret Detection / Gitleaks + Trufflehog**: [Trufflehog Demo @ 21m0s](https://chorus.ai/meeting/2006293?tab=summary&call=ABC00A56EBBF4037AA1356CF1DA51B38)
* **IAST**: [Demo](#)
* **Fuzzing**: [Demo](#)
* **Dependency Scanning / Gemnasium + Language Specific**: [Demo](#)
* **Container Scanning / Clair + klar**: [Demo](#)
* **License Compliance**: [Demo](#)

#### Release

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/release/)

* **Continuous Delivery**: [Demo](#)
* **Release Orchestration**: [Demo](#)
* **Pages**: [Handbook](https://jyavorska.gitlab.io/gl-handbook-news/) | [Handbook](https://mbruemmer-projects.gitlab.io/better-handbook-changelog/) | [What's New](https://mbruemmer.gitlab.io/what-is-new-since/) | [Team Page](https://gitlab-com.gitlab.io/teampage-map/) | [Maintainer Workload](https://leipert-projects.gitlab.io/maintainer-workload/)
* **Review Apps**: [Demo](#)
* **Incremental Rollout**: [Demo](#)
* **Feature Flags / Unleash**: [Python Project](https://gitlab.com/gitlab-gold/tpoffenbarger/auto-devops/django-auto-devops) | [Demo](https://gitlab.gitlab.ninja/cluster/feature-flags/demo) | [Demo]()
* **Release Governance**: [Internal-only Demo](https://gitlab-core.us.gitlabdemo.cloud/xgdemo/minimal-ruby-app)
* **Secrets Management / Vault**: [Vault Demo](https://www.youtube.com/watch?v=F-YO1ajaNPE&feature=youtu.be)

## Operations UX

#### Configure

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/configure/)

* **Auto DevOps**: [Demo](#)
* **Kubernetes Management**: [Demo](#)
* **ChatOps / Slack**: [Demo](#)
* **Runbooks / Jupyter + Rubix**: [Demo](#)
* **Serverless / Knative**: [Demo](#) | [Samples](https://gitlab.com/gitlab-examples?utf8=%E2%9C%93&filter=knative)
* **Infrastructure as Code / Terraform**: [Demo](#)
* **Chaos Engineering / Litmus**: [Demo](#)
* **Cluster Cost Optimization**: [Demo](#)

#### Monitor 

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/monitor/)

* **Metrics / Prometheus**: [Demo](#)
* **Logging**: [Demo](#)
* **Tracing / Jaeger**: [Demo](#)
* **Cluster Monitoring**: [Demo](#)
* **Error Tracking / Sentry**: [Demo](#)
* **Synthetic Monitoring**: [Demo](#)
* **Incident Management**: [Demo](#)
* **Status Page**: [Demo](#)

#### Defend

[Up-to-date List of Categories](https://about.gitlab.com/stages-devops-lifecycle/defend/)

* **WAF**: [Demo](#)
* **Threat Detection**: [Demo](#)
* **Vulnerability Management**: [Demo](#)
* **Container Network Security**: [Demo](#)
* **RASP**: [Demo](#)
* **UEBA**: [Demo](#)
* **DLP**: [Demo](#)
